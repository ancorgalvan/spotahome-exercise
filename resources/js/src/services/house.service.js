const APP_URL = 'http://127.0.0.1:8000/api'

export const houseService = {
    getHouses,
}

export function getHouses ({offset, limit}) {
  const requestOptions = {
    method: 'GET',
  }

  return fetch(APP_URL + '/houses?offset=' + offset +'&limit=' + limit, requestOptions)
      .then(
          response => response.json()
      )
      .catch(
          error => Promise.reject(error)
      )
}

