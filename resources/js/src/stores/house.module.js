
import { houseService } from '../services'
import House from "../models/House";

export const houses = {
    namespaced: true,
    state: {
        houses: [],
        limit: 25,
        offset: 1,
        total: 0,
    },
    actions: {
        async loadHouses ({dispatch, commit}, payload) {
            try {
                const response = await houseService.getHouses(payload)
                const houses = response.data.houses.map(house => new House({
                    title: house.title,
                    link: house.link,
                    address: house.address,
                    city: house.city,
                    image: house.image,
                }))
                commit('addHouses', houses)
                commit('setLimits', {
                    limit: response.data.limit,
                    offset: response.data.offset,
                    total: response.data.total,
                })
            } catch (error) {
                commit('actionFailure', error)
            }
        },
    },
    mutations: {
        addHouses (state, houses) {
            state.houses = houses
        },
        setLimits (state, {limit, offset, total}) {
            state.limit = limit
            state.offset = offset
            state.total = total
        },
        actionFailure (state, error) {
            console.log(error)
            state.houses = []
        },
    },
}
