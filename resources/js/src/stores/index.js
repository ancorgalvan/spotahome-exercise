import Vue from 'vue'
import Vuex from 'vuex'

import { houses } from './house.module'

Vue.use(Vuex)


export const store = new Vuex.Store({
    modules: {
      houses,
    },
})
