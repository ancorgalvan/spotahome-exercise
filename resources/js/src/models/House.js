export default class House {
  constructor ({
      title= '',
      link = null,
      address = '',
      city = '',
      image = null,
     }) {
      this.title = title
      this.link = link
      this.address = address
      this.city = city
      this.image = image
  }
}
