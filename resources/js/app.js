require('./bootstrap');

import Vue from "vue";
import App from "./src/App.vue";
import { store } from "./src/stores";

new Vue({
    store,
    render: h => h(App),
}).$mount("#app");
