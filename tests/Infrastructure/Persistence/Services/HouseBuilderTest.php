<?php declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Services;

use App\Domain\Models\House;
use App\Infrastructure\Persistence\Services\HouseBuilder;
use Tests\TestCase;

class HouseBuilderTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testBuilder(): void
    {
        $houseData = json_decode(file_get_contents(__DIR__ . '/Data/data.json'), true);
        $house = HouseBuilder::fromData($houseData[0]);

        $this->assertInstanceOf(House::class, $house);
        $this->assertEquals("House 79906", $house->getTitle());
        $this->assertEquals("https://www.spotahome.com/madrid/for-rent:apartments/79906?utm_source=housinganywhere&utm_medium=cffeeds&utm_campaign=normalads", $house->getLink());
        $this->assertEquals("Paseo de la Castellana", $house->getAddress());
        $this->assertEquals("madrid", $house->getCity());
        $this->assertEquals("https://d1052pu3rm1xk9.cloudfront.net/fsosw_960_540_verified_ur_6_50/74ef00db848c1ce44ea03677836b5bb61243e3f9d6a9d5a656d3c3cb.jpg", $house->getImage()->getValue());
    }
}
