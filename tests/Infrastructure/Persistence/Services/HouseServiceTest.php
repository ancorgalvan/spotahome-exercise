<?php declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Services;

use App\Domain\Collections\HouseCollection;
use App\Infrastructure\Persistence\Services\HouseServices;
use Tests\TestCase;

class HouseServiceTest extends TestCase
{
    private HouseServices $houseServices;

    protected function setUp(): void
    {
        parent::setUp();
        $this->houseServices = $this->app->get(HouseServices::class);
    }

    public function testGetters(): void
    {
        $houseCollection = $this->houseServices->getHouseCollection();

        $this->assertInstanceOf(HouseCollection::class, $houseCollection);
        $this->assertEquals($houseCollection->count(), $this->houseServices->count());
    }
}
