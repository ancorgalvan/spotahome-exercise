<?php declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Repository;

use App\Infrastructure\Persistence\Repository\HouseDataRepository;
use Illuminate\Pagination\Paginator;
use Tests\TestCase;

class HouseDataRepositoryTest extends TestCase
{
    private HouseDataRepository $houseDataRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->houseDataRepository = $this->app->get(HouseDataRepository::class);
    }

    public function testMatchAll()
    {
        $houseCollection = $this->houseDataRepository->match(20, 1);

        $this->assertInstanceOf(Paginator::class, $houseCollection);
        $this->assertEquals(20, $houseCollection->count());
    }
}
