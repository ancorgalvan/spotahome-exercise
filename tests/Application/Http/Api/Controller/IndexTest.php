<?php declare(strict_types=1);


namespace Tests\Application\Http\Api\Controller;

use App\Infrastructure\Http\Api\Controller\HouseController;
use Tests\TestCase;


/** @covers \App\Infrastructure\Http\Api\Controller\HouseController */
class IndexTest extends TestCase
{

    public function testIndex(): void
    {
        $response = $this->getJson(
            action([HouseController::class, 'index']), [
                'elements' => 25,
                'page' => 1
            ]);

        $response->assertOk();

    }


}
