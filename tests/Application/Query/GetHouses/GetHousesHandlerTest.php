<?php declare(strict_types=1);

namespace Tests\Application\Query\GetHouses;

use App\Application\Query\GetHouses\GetHousesHandler;
use App\Application\Query\GetHouses\GetHousesQuery;
use App\Application\Query\GetHouses\GetHousesResponse;
use App\Domain\Collections\HouseCollection;
use App\Domain\Models\House;
use App\Domain\Models\Link;
use App\Domain\Repository\HouseRepository;
use App\Infrastructure\Persistence\Services\ObjectPaginator;
use Tests\TestCase;


class GetHousesHandlerTest extends TestCase
{
    private GetHousesHandler $handler;
    private HouseRepository $houseRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->houseRepository = $this->createMock(HouseRepository::class);
        $this->handler = new GetHousesHandler($this->houseRepository);
    }

    public function test_handle_returns_proper_response_object()
    {
        $house = new House('House 1', new Link('https://localhost/house1'), 'Address', 'City', null);
        $houseCollection = new HouseCollection([$house]);
        $paginator = new ObjectPaginator($houseCollection, 25, 1);
        $this->houseRepository->expects($this->once())->method('match')->willReturn($paginator);
        $this->houseRepository->expects($this->once())->method('count')->willReturn(1);
        $query = new GetHousesQuery(25, 1);
        $results = $this->handler->handle($query);

        $this->assertInstanceOf(GetHousesResponse::class, $results);
        $this->assertEquals(new GetHousesResponse($paginator, 1), $results);
    }
}
