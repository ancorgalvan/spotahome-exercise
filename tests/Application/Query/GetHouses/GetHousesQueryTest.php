<?php declare(strict_types=1);

namespace Tests\Application\Query\GetHouses;

use App\Application\Query\GetHouses\GetHousesQuery;
use PHPUnit\Framework\TestCase;

class GetHousesQueryTest extends TestCase
{
    private GetHousesQuery $query;

    public function setUp(): void
    {
        parent::setUp();
        $this->query = new GetHousesQuery(25, 3);
    }

    public function test_getters()
    {
        $this->assertEquals(25, $this->query->getElementsPerPage());
        $this->assertEquals(3, $this->query->getPage());
    }

    public function test_is_json_serializable()
    {
        $expectedArray = [
            'query' => 'GetHousesQuery',
            'params' => ['elementsPerPage' => 25, 'page' => 3],
        ];
        $this->assertEquals($expectedArray, $this->query->jsonSerialize());
        $this->assertJsonStringEqualsJsonString(json_encode($expectedArray), json_encode($this->query));
    }
}
