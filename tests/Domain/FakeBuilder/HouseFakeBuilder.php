<?php declare(strict_types=1);

namespace Tests\Domain\FakeBuilder;

use App\Domain\Models\House;
use App\Domain\Models\Image;
use App\Domain\Models\Link;
use Tests\FakeBuilder;

class HouseFakeBuilder extends FakeBuilder
{
    private string $title;
    private Link $link;
    private string $address;
    private string $city;
    private Image $image;


    public function random(): self
    {
        $this->title = $this->getFaker()->name;
        $this->link = new Link($this->getFaker()->url);
        $this->address = $this->getFaker()->address;
        $this->city = $this->getFaker()->city;
        $this->image = new Image($this->getFaker()->url);

        return $this;
    }

    public function withTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function withLink(Link $link): self
    {
        $this->link = $link;
        return $this;
    }

    public function withAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    public function withCity(string $city): self
    {
        $this->city = $city;
        return $this;
    }

    public function withImage(Image $image): self
    {
        $this->image = $image;
        return $this;
    }


    public function generate(): House
    {
        return new House(
            $this->title,
            $this->link,
            $this->address,
            $this->city,
            $this->image
        );

    }


}
