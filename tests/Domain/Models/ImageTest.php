<?php declare(strict_types=1);

namespace Tests\Domain\Models;

use App\Domain\Exception\ImageException;
use App\Domain\Models\Image;
use Tests\TestCase;

class ImageTest extends TestCase
{

    public function testValid()
    {
        $url = 'https://localhost/valid';
        $image = new Image($url);
        $this->assertEquals($url, $image->getValue());
    }

    public function testInvalid()
    {
        $this->expectException(ImageException::class);
        $url = 'https//localhost/valid';
        $image = new Image($url);
    }
}