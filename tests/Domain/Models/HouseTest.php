<?php declare(strict_types=1);

namespace Tests\Domain\Models;

use App\Domain\Models\House;
use App\Domain\Models\Image;
use App\Domain\Models\Link;
use Tests\Domain\FakeBuilder\HouseFakeBuilder;
use Tests\TestCase;


class HouseTest extends TestCase
{
    private HouseFakeBuilder $houseFakeBuilder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->houseFakeBuilder = $this->app->get(HouseFakeBuilder::class);
    }

    public function testGenerate()
    {
        $house = $this->houseFakeBuilder->generate();
        $this->assertInstanceOf(House::class, $house);
    }

    public function testGetters(): void
    {

        $link = new Link('https://localhost/link');
        $image = new Image('https://localhost/link');
        $house = $this->houseFakeBuilder
            ->withTitle('House')
            ->withAddress('Calle Test')
            ->withCity('Ciudad')
            ->withLink($link)
            ->withImage($image)
            ->generate();

        $this->assertEquals('House', $house->getTitle());
        $this->assertEquals('Calle Test', $house->getAddress());
        $this->assertEquals('Ciudad', $house->getCity());
        $this->assertEquals($link, $house->getLink());
        $this->assertEquals($image, $house->getImage());
    }
}
