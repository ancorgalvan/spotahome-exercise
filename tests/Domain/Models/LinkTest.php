<?php declare(strict_types=1);

namespace Tests\Domain\Models;

use App\Domain\Exception\LinkException;
use App\Domain\Models\Link;
use Tests\TestCase;

class LinkTest extends TestCase
{

    public function testValid()
    {
        $url = 'https://localhost/valid';
        $link = new Link($url);
        $this->assertEquals($url, $link->getValue());
    }

    public function testInvalid()
    {
        $this->expectException(LinkException::class);
        $url = 'https//localhost/valid';
        $link = new Link($url);
    }
}