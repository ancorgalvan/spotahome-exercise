<?php

declare(strict_types=1);

namespace App\Domain\Collections;

use App\Domain\Models\House;

class HouseCollection extends ObjectCollection
{
    /**
     * @return House[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->items);
    }

    public static function allowedObjectClass(): string
    {
        return House::class;
    }

    protected function itemAssertions($item): void
    {
    }

    public function jsonSerialize(): array
    {
        return array_map(
            fn (House $house) => $house->jsonSerialize(),
            $this->getItems()
        );
    }
}
