<?php

namespace App\Domain\Models;

class House
{
    private string $title;
    private Link $link;
    private string $address;
    private string $city;
    private ?Image $image;

    public function __construct(string $title, Link $link, string $address, string $city, ?Image $image)
    {
        $this->title = $title;
        $this->link = $link;
        $this->address = $address;
        $this->city = $city;
        $this->image = $image;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): Link
    {
        return $this->link;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function jsonSerialize(): array
    {
        return [
            'title' => $this->title,
            'address' => $this->address,
            'city' => $this->city,
            'link' => $this->link->getValue(),
            'image' => is_null($this->image) ? null : $this->image->getValue(),
        ];
    }
}
