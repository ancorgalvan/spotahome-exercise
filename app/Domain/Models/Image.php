<?php

declare(strict_types=1);

namespace App\Domain\Models;

use App\Domain\Exception\ImageException;
use App\Domain\Exception\LinkException;

class Image
{
    private string $url;

    public function __construct(string $url)
    {
        $this->assertValidUrl($url);
        $this->url = $url;
    }

    public function getValue(): string
    {
        return $this->url;
    }

    private function assertValidUrl(string $url): void
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw ImageException::invalidUrl($url);
        }
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}
