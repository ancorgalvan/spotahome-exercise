<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Collections\HouseCollection;
use App\Infrastructure\Persistence\Services\ObjectPaginator;
use Illuminate\Contracts\Pagination\Paginator;

interface HouseRepository
{
    public function match(int $elementsPerPage, int $page): ObjectPaginator;

    public function count(): int;
}
