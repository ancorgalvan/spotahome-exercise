<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use Exception;

class LinkException extends Exception implements DomainException
{
    public static function invalidUrl(string $url): self
    {
        return new self(sprintf('Invalid url "%s"', $url));
    }
}
