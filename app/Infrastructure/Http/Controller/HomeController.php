<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller;

class HomeController
{
    public function index()
    {
        return view('home');
    }
}
