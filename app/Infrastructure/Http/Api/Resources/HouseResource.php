<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Api\Resources;

use App\Domain\Models\House;
use App\Infrastructure\Http\Api\JsonApiResource;

class HouseResource extends JsonApiResource
{
    private int $total;

    public function __construct($resource, int $total, int $statusCode = null)
    {
        parent::__construct($resource, $statusCode);
        $this->total = $total;
    }

    public function toArray($request): array
    {
        return [
            'houses' => array_map(fn (House $house) => $house->jsonSerialize(), $this->resource->items()),
            'total' => $this->total,
            'offset' => $request->get('offset'),
            'limit'=> $request->get('limit')
        ];
    }
}
