<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Api\Controller;

use App\Application\Query\GetHouses\GetHousesQuery;
use App\Domain\Bus\QueryBus;
use App\Domain\Repository\HouseRepository;
use App\Infrastructure\Http\Api\Resources\HouseResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HouseController
{
    public function index(Request $request, QueryBus $queryBus): HouseResource
    {
        $query = new GetHousesQuery(
            (int) $request->get('limit', 25),
            (int) $request->get('offset', 1)
        );
        $houses = $queryBus->handle($query);

        return new HouseResource($houses->getItems(), $houses->getTotal(), Response::HTTP_OK);
    }
}
