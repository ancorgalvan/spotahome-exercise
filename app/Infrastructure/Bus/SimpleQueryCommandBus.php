<?php

declare(strict_types=1);

namespace App\Infrastructure\Bus;

use App\Domain\Bus\Query;
use App\Domain\Bus\QueryBus;
use App\Domain\Bus\QueryResponse;
use Illuminate\Contracts\Container\Container;
use InvalidArgumentException;

class SimpleQueryCommandBus implements QueryBus
{
    private array $handlers;
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->handlers = [];
    }

    public function addHandler($commandName, $handler): void
    {
        $this->handlers[$commandName] = $handler;
    }

    public function handle(Query $query): QueryResponse
    {
        $commandHandler = $this->handlers[get_class($query)];
        if ($commandHandler === null) {
            throw new InvalidArgumentException();
        }

        $handler = $this->container->make($commandHandler);
        return $handler->handle($query);
    }
}
