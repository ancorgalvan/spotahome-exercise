<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Repository;

use App\Domain\Repository\HouseRepository;
use App\Infrastructure\Persistence\Services\HouseServices;
use App\Infrastructure\Persistence\Services\ObjectPaginator;

class HouseDataRepository implements HouseRepository
{
    private HouseServices $houseService;

    public function __construct(HouseServices $houseServices)
    {
        $this->houseService = $houseServices;
    }

    public function match(int $elementsPerPage, int $page): ObjectPaginator
    {
        $houseCollection = $this->houseService->getHouseCollection();

        return new ObjectPaginator($houseCollection, $elementsPerPage, $page);
    }

    public function count(): int
    {
        return $this->houseService->count();
    }
}
