<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Services;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class ObjectPaginator extends Paginator
{
    protected function setItems($items)
    {
        $this->items = $items instanceof Collection ? $items : Collection::make($items);

        $this->hasMore = $this->items->count() > $this->perPage;

        $this->items = $this->items->slice($this->currentPage, $this->perPage);
    }
}
