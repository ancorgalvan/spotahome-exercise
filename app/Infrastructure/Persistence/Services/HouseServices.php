<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Services;

use App\Domain\Collections\HouseCollection;

class HouseServices
{
    private static HouseCollection $houseCollection;

    public function __construct()
    {
        $datos = json_decode(file_get_contents(__DIR__ . '/Data/houses-data.json'), true);

        self::$houseCollection = new HouseCollection(
            array_map(
                fn (array $data) => HouseBuilder::fromData($data),
                $datos
            )
        );
    }

    public function getHouseCollection(): HouseCollection
    {
        return self::$houseCollection;
    }

    public function count(): int
    {
        return count(self::$houseCollection);
    }
}
