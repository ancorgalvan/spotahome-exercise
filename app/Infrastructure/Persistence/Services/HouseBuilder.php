<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Services;

use App\Domain\Models\House;
use App\Domain\Models\Image;
use App\Domain\Models\Link;

class HouseBuilder
{
    private const PREFIX_TITLE = 'House ';

    public static function fromData(array $houseData): House
    {
        $image = (count($houseData['Images']) > 0) ? reset($houseData['Images']) : null;
        return new House(
            self::PREFIX_TITLE . $houseData['ListingReference'],
            new Link($houseData['Link']),
            $houseData['Address'],
            $houseData['City'],
            $image ? new Image($image) : null
        );
    }
}
