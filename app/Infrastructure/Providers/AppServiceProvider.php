<?php

namespace App\Infrastructure\Providers;

use App\Domain\Repository\HouseRepository;
use App\Infrastructure\Persistence\Repository\HouseDataRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HouseRepository::class, HouseDataRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
