<?php

declare(strict_types=1);

namespace App\Infrastructure\Providers;

use App\Application\Command\Sample\SampleCommand;
use App\Application\Command\Sample\SampleHandler;
use App\Application\Command\UploadedRidersFileCommand;
use App\Application\Command\UploadedRidersFileHandler;
use App\Application\Query\GetHouses\GetHousesHandler;
use App\Application\Query\GetHouses\GetHousesQuery;
use App\Domain\Bus\CommandBus;
use App\Domain\Bus\QueryBus;
use App\Infrastructure\Bus\SimpleCommandBus;
use App\Infrastructure\Bus\SimpleQueryCommandBus;
use Illuminate\Support\ServiceProvider;

class BusServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        // Query bus
        $this->app->bind(QueryBus::class, function () {
            $cb = new SimpleQueryCommandBus($this->app);
            $cb->addHandler(GetHousesQuery::class, GetHousesHandler::class);
            return $cb;
        });
    }
}
