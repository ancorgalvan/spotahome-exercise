<?php

declare(strict_types=1);

namespace App\Application\Query\GetHouses;

use App\Domain\Repository\HouseRepository;

class GetHousesHandler
{
    private HouseRepository $houseRepository;

    public function __construct(HouseRepository $houseRepository)
    {
        $this->houseRepository = $houseRepository;
    }

    public function handle(GetHousesQuery $query): GetHousesResponse
    {
        $elementsPerPage = $query->getElementsPerPage();
        $page = $query->getPage();

        $housePaginated = $this->houseRepository->match($elementsPerPage, $page);
        $total = $this->houseRepository->count();

        return new GetHousesResponse($housePaginated, $total);
    }
}
