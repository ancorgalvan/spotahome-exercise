<?php

declare(strict_types=1);

namespace App\Application\Query\GetHouses;

use App\Domain\Bus\QueryResponse;
use Illuminate\Contracts\Pagination\Paginator;

class GetHousesResponse implements QueryResponse
{
    private Paginator $collectionPaginated;
    private int $total;

    public function __construct(Paginator $paginator, int $total)
    {
        $this->collectionPaginated = $paginator;
        $this->total = $total;
    }

    public function getItems(): Paginator
    {
        return $this->collectionPaginated;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
