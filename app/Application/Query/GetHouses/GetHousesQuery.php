<?php

declare(strict_types=1);

namespace App\Application\Query\GetHouses;

use App\Domain\Bus\Query;

class GetHousesQuery implements Query
{
    private int $elementsPerPage;
    private int $page;

    public function __construct(int $elementsPerPage, int $page)
    {
        $this->elementsPerPage = $elementsPerPage;
        $this->page = $page;
    }

    public function getElementsPerPage(): int
    {
        return $this->elementsPerPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function jsonSerialize(): array
    {
        return [
            'query' => 'GetHousesQuery',
            'params' => [
                'elementsPerPage' => $this->elementsPerPage,
                'page' => $this->page
            ],
        ];
    }
}
