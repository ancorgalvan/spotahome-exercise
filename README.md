# Soptahome Exercise

Exercise development in Laravel and Vue

## Mount the project

Install vendors of laravel
```
# cd spotahome-exercise
# composer instal
```

Install components of Vue
```
# npm install
# npm run production
```

## Run server

```
# php artisan server
```

## Access to next url
Sometimes run the server in other ports, it will indicate in console

```
http://127.0.0.1:8000/
``` 

## Launch the test
```
# composer phpunit
```